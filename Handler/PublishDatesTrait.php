<?php
namespace Rup\Bundle\CoreBundle\Handler;

use Rup\Bundle\CoreBundle\Model\ManageDate\DateInterface;

/**
 * Class PublishDatesTrait
 *
 * @package Rup\Bundle\CoreBundle\Handler
 */
trait PublishDatesTrait
{
    /**
     * @param $entity
     * @param $entityDto
     */
    protected function updatePublishDates(DateInterface $entity, $entityDto)
    {
        if ($entityDto->published != $entity->isPublished()) {
            if ($entityDto->published) {
                $entity->setPublishDate(time());
            } elseif ($entityDto->publishDate) {
                $entity->setCancelPublishDate(time());
            }
        }
    }
} 