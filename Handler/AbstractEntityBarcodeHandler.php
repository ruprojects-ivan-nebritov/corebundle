<?php
namespace Rup\Bundle\CoreBundle\Handler;

use Rup\Bundle\CoreBundle\Exception\EntityBarcode\EntityBarcodeParseException;
use Rup\Bundle\CoreBundle\Exception\EntityBarcode\UnableToGenerateEntityBarcodeException;
use Rup\Bundle\CoreBundle\Exception\Type\TypeException;
use Rup\Bundle\CoreBundle\Util\Type\AbstractStatusType;
use Rup\Bundle\CoreBundle\Util\Type\StatusTypeInterface;
use Doctrine\ORM\EntityManager;

/**
 * Class AbstractEntityBarcodeHandler
 *
 * @package Rup\Bundle\CoreBundle\Handler
 */
abstract class AbstractEntityBarcodeHandler extends AbstractStatusType implements StatusTypeInterface
{
    /**
     * @var string
     */
    protected $delimiter;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param EntityManager $entityManager
     * @param string $delimiter
     */
    public function __construct(EntityManager $entityManager, $delimiter = '-')
    {
        $this->entityManager = $entityManager;
        $this->delimiter = $delimiter;
    }

    /**
     * @param EntityBarcodeInterface $entity
     *
     * @return string
     *
     * @throws UnableToGenerateEntityBarcodeException
     */
    public function generate(EntityBarcodeInterface $entity)
    {
        $className = get_class($entity);

        if (!$barcodeCommonPart = $this->getKeyByType($className)) {
            throw new UnableToGenerateEntityBarcodeException("Unable to generate entity barcode for class {$className}.");
        }

        if (!$entity->getId()) {
            throw new UnableToGenerateEntityBarcodeException("Unable to generate entity barcode. Entity {$className} has no id.");
        }

        $barcode = $this->encodeBarcode(array($barcodeCommonPart, $entity->getId()));

        $entity->setBarcode($barcode);

        return $barcode;
    }

    /**
     * @param $barcode
     *
     * @return object
     *
     * @throws EntityBarcodeParseException
     */
    public function getEntityByBarcode($barcode)
    {
        $barcodeParts = $this->parseBarcode($barcode);

        if (count($barcodeParts) < 2) {
            throw new EntityBarcodeParseException();
        }

        try {
            $class = $this->getTypeByKey($barcodeParts['class']);
        } catch (TypeException $exception) {
            throw new EntityBarcodeParseException();
        }

        if (!$class) {
            throw new EntityBarcodeParseException();
        }

        $entityRepository = $this->entityManager->getRepository($class);

        if (!$entity = $entityRepository->find($barcodeParts['id'])) {
            throw new EntityBarcodeParseException();
        }

        return $entity;
    }

    /**
     * @param $barcode
     *
     * @return array
     */
    public function parseBarcode($barcode)
    {
        $barcodeParts = explode($this->delimiter, $barcode);

        return array(
            'class' => array_shift($barcodeParts),
            'id' => array_shift($barcodeParts),
            'args' => $barcodeParts
        );
    }

    /**
     * @param array $barcodeParts
     * @param null|string $delimiter
     *
     * @return string
     * 
     * @throws UnableToGenerateEntityBarcodeException
     */
    protected function encodeBarcode($barcodeParts, $delimiter = null)
    {
        if ($delimiter) {
            $this->delimiter = $delimiter;
        }

        if (!is_array($barcodeParts) || count($barcodeParts) < 2) {
            throw new UnableToGenerateEntityBarcodeException('Unable to generate entity barcode. Barcode must consists not less than two parts.');
        }

        return implode($this->delimiter, $barcodeParts);
    }
}