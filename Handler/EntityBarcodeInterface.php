<?php
namespace Rup\Bundle\CoreBundle\Handler;

/**
 * Interface EntityBarcodeInterface
 *
 * @package Rup\Bundle\CoreBundle\Handler
 */
interface EntityBarcodeInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param string $barcode
     *
     * @return EntityBarcodeInterface
     */
    public function setBarcode($barcode);

    /**
     * @return string
     */
    public function getBarcode();
}