<?php
namespace Rup\Bundle\CoreBundle\HttpFoundation;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class PDFResponse
 *
 * @package Rup\Bundle\CoreBundle\HttpFoundation
 */
class PDFResponse extends Response
{
    /**
     * @param string $content
     * @param int $status
     * @param array  $headers
     */
    public function __construct($content = '', $status = 200, $headers = array())
    {
        parent::__construct($content, $status, $headers);

        $this->headers->set('Content-Type', 'application/pdf');
    }

    /**
     * @param string $name
     */
    public function setFileName($name)
    {
        $this->headers->set('Content-Disposition', 'inline; filename="'.basename($name).'"');
    }
} 