<?php
namespace Rup\Bundle\CoreBundle\HttpFoundation;

use Rup\Bundle\CoreBundle\Utils\JsonUtils;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StrictJsonResponse
 *
 * @package Rup\Bundle\CoreBundle\HttpFoundation
 */
class StrictJsonResponse extends Response
{

    /**
     * @param mixed $data data to send at response
     */
    public function __construct($data)
    {
        parent::__construct($this->toJson($data));
    }

    /**
     * @param mixed $data
     *
     * @return string
     */
    function toJson($data)
    {
        return json_encode(JsonUtils::prepare($data));
    }
}