<?php
namespace Rup\Bundle\CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class OverridePass
 *
 * @package Rup\Bundle\CoreBundle\DependencyInjection\Compiler
 */
class OverridePass implements CompilerPassInterface
{
    /**
     * @inheritdoc
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->has('jms_serializer')) {
            $container->setParameter(
                'jms_serializer.unserialize_object_constructor.class',
                'Rup\Bundle\CoreBundle\Services\Override\JMSSerializer\Construction\ObjectEmptyConstructor'
            );

            $container->setParameter(
                'jms_serializer.json_deserialization_visitor.class',
                'Rup\Bundle\CoreBundle\Services\Override\JMSSerializer\JsonDeserializationVisitor'
            );
        }
    }
}
