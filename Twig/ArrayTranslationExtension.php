<?php
namespace Rup\Bundle\CoreBundle\Twig;

use Symfony\Component\Translation\TranslatorInterface;
use Twig_Error_Runtime;
use Twig_Extension;
use Twig_SimpleFilter;

/**
 * Class ArrayTranslationExtension
 */
class ArrayTranslationExtension extends Twig_Extension
{
    /**
     * Constructs the service
     *
     * @param TranslatorInterface $translator Translation service implementing the translation interface
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('transArray', array($this, 'transArrayFilter')),
        );
    }

    /**
     * Translates each element of an array using the provided translator
     *
     * @param array  $array      An array of message ids (may also be an array of objects that can be cast to string)
     * @param array  $parameters An array of parameters for the messages
     * @param string $domain     The domain for the messages
     * @param string $locale     The locale
     *
     * @return array An array of (hopefully) translated messages
     *
     * @throws Twig_Error_Runtime
     */
    public function transArrayFilter(array $array, array $parameters = array(), $domain = null, $locale = null)
    {
        $result = [];

        foreach ($array as $item) {
            $result[] = $this->translator->trans($item, $parameters, $domain, $locale);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'translation_extension';
    }
}
