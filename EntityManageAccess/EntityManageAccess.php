<?php
namespace Rup\Bundle\CoreBundle\EntityManageAccess;

/**
 * Class EntityManageAccess
 *
 * @package Rup\Bundle\CoreBundle\EntityManageAccess
 */
class EntityManageAccess
{
    /**
     * @var bool
     */
    public $isAddable = false;

    /**
     * @var bool
     */
    public $isEditable = false;

    /**
     * @var bool
     */
    public $isDeletable = false;
} 