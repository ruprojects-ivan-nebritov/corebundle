<?php
namespace Rup\Bundle\CoreBundle\Dto;

/**
 * Class PublishedDtoTrait
 *
 * @package Rup\Bundle\CoreBundle\Dto
 */
trait PublishedDtoTrait
{
    /**
     * @var boolean $createDate
     */
    public $published;
}