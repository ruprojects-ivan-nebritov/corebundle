/**
 * @param dialogSelector
 * @param options = {
 *     getFormUrl:         '/get/form',
 *     sendFormUrl         '/send/form',
 *     data                '{ params: $('#filterForm').serialize() }'
 *     modalId:            '#modal',
 *     buttonSelector:     '#button',
 *     submitFormSelector: '#buttonSubmit',
 *     closeSelector:      '.close-modal',
 *     success:            'function(data, modal) {}'
 *     onSuccessHtml       'function(data, modal) {}'
 * }
 *
 * @constructor
 */
function FormModal(dialogSelector, options)
{
    var self = this;

    this.defaults = {
        getFormUrl: null,
        sendFormUrl: null,
        data: null,
        buttonSelector: null,
        submitFormSelector: null,
        closeSelector: '.close-modal',
        modalId: '#modal',
        success: function(data, modal) {
            modal.closeModal();
            location.reload();
        },
        onSuccessHtml: function(data, modal) {
            modal.$dialog.html(data);
            $(self.modalId).modal('show');
        }
    };

    init(options);

    this.$dialog = $(self.modalId).find(dialogSelector);
    this.$submitButton = $(self.modalId).find(self.submitFormSelector);
    this.isActive = false;
    this.contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
    this.processData = true;
    this.sendData = null;

    $(document).on('click', self.buttonSelector, function(e) {
        e.preventDefault();
        self.getFormData();
    });

    $(this.modalId).on('click', self.closeSelector, function() {
        self.closeModal();
    });

    $(this.modalId).on('click', self.submitFormSelector, function(e) {
        e.preventDefault();
        if (self.isActive) {
            return;
        }
        $(this).addClass('disabled');
        self.sendForm(self.sendFormUrl);
    });

    $(this.modalId).on('keypress', function(event) {
        if(event.keyCode == 13) {
            event.preventDefault();
            self.enterHandler();
        }
    });

    function init(options) {
        for (var name in self.defaults) {
            self[name] = (options !== undefined && options[name] !== undefined)
                ? options[name]
                : self.defaults[name];
        }
    }
}

FormModal.prototype = {

    getFormData: function() {
        var self = this;

        new Ajaxer({
            url: self.getFormUrl,
            data: self.data,
            onSuccess: function(data) {
                self.success(data, self);
            },
            onSuccessHtml: function(data) {
                self.onSuccessHtml(data, self);
            }
        });
    },

    sendForm: function(url) {
        var self = this;

        self.isActive = true;

        if ($(self.getFormSelector()).find('input[type="file"]')) {
            self.contentType = false;
            self.processData = false;
            self.sendData = new FormData($(self.getFormSelector())[0]);
        } else {
            self.sendData = $(self.getFormSelector()).serialize();
        }

        new Ajaxer({
            url: self.sendFormUrl,
            type: 'POST',
            contentType: self.contentType,
            processData: self.processData,
            data: self.sendData,
            onSuccess: function(data) {
                self.success(data, self);
            },
            onSuccessHtml: function(data) {
                self.onSuccessHtml(data, self);
            },
            complete: function(xhr) {
                $(self.submitFormSelector).removeClass('disabled');
                self.isActive = false;
            }
        });
    },

    getFormSelector: function() {
        return this.$dialog.find('form');
    },

    closeModal: function() {
        $(this.modalId).modal('hide');
    },

    enterHandler: function() {
        this.$submitButton.click();
    }
};
