function FlashMessages(container, timeOut) {

    var self = this;

    this.$container = container ? $(container) : $('.flash-messages');
    this.messageContainer = '.alert-dismissable';
    this.timeOut = timeOut ? timeOut : 5000;
    this.wrapper = '.flash-messages-wrapper';
    this.$wrapper = this.$container.find(this.wrapper);
    this.timeOutId = null;

    this.messageDiv = '<div class="alert alert-dismissable alert-{{type}} flash text-center">{{message}}</div>';

    this.$container.find('.close').on('click', function() {
        self.hideContainer();
    });

    this.init();

}

FlashMessages.prototype = {

    init: function() {
        var self = this;

        if (this.timeOutId) {
            clearTimeout(this.timeOutId);
        }

        if (this.$container.find(this.messageContainer).length) {
            this.$container.css({
                bottom: - this.$container.outerHeight()
            }).show();

            this.showContainer();

            this.timeOutId = setTimeout(function() {
                self.hideContainer();
            }, this.timeOut);
        }
    },

    showContainer: function() {
        this.$container.animate({
            bottom: 0
        })
    },

    hideContainer: function() {
        var self = this;

        this.$container.animate({
            bottom: - this.$container.outerHeight()
        },
        function() {
            $(this).hide();
            self.clearWrapper();
        })
    },

    clearWrapper: function() {
        this.$wrapper.empty();
    },

    addMessage: function(message, type) {
        type = type ? type : 'danger';

        var messageDiv = this.messageDiv.replace('{{message}}', message).replace('{{type}}', type);

        this.$wrapper.append(messageDiv);

        this.init();
    }

};