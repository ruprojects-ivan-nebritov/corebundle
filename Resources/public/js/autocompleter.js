function Autocompleter(textInput, valueInput, url, options, isCache) {
    this.defaults = {
        minLength: 2,
        property: 'name',
        idProperty: 'id',
        autoFocus: false
    };

    this.init(options);
    if(typeof(isCache) == 'undefined') {
        isCache = true;
    }
    this.isCache = isCache;

    this.onSelect = function(){};
    this.onChange = function(){};

    var self = this;
    this.$textInput = $(textInput);
    this.$valueInput = $(valueInput);
    this.url = url;

    this.cache = {};

    this.urlParameters = {};

    this.current = null;

    this.$autocomplete = this.$textInput.autocomplete({
        minLength: self.minLength,
        autoFocus: self.autoFocus,
        source: function (request, response) {
            var term = request.term;
            if (self.isCache) {
                if (term in self.cache) {
                    response(self.cache[term]);
                    return;
                }
            }

            request = $.extend(request, self.urlParameters);

            $.getJSON(self.url, request, function (data) {
                self.cache[term] = data;
                response(data);
            });
        },
        select: function (event, ui) {
            $(this).val(ui.item[self.property]);
            self.$valueInput.val(ui.item[self.idProperty]);
            $(this).blur();
            self.current = ui.item;
            self.onSelect(ui.item);

            return false;
        }
    }).data('ui-autocomplete')._renderItem = function (ul, item) {
        return $('<li>')
            .append('<a>' + item[self.property] + '</a>')
            .appendTo(ul);
    };

    //show autocomplete list on focus
    this.$textInput.focus(function () {
        var term = $(this).val();
        if (term.length > 1) {
            $(this).autocomplete('search', term);
        }
    });

    //clear value after changing text
    this.$textInput.on('keyup input', function(e) {
        if (self.$valueInput.val()) {
            self.$valueInput.val('');
            self.onChange();
        }
    });
}

Autocompleter.prototype = {
    init : function(options) {
        for (var name in this.defaults) {
            this[name] = (options !== undefined && options[name] !== undefined)
                ? options[name]
                : this.defaults[name];
        }
    },

    getValueInputValue: function() {
        return this.$valueInput.val();
    },

    getValueInputValue: function() {
        return this.$valueInput.val();
    },

    setTextInputValue: function(value) {
        this.$textInput.val(value);
    },

    setValueInputValue: function(value) {
        this.$valueInput.val(value);
    },

    setUrlParameters: function(parameters) {
        this.urlParameters = parameters;
    },

    setOnSelect: function(onSelect) {
        this.onSelect = onSelect;
    },

    setOnChange: function(onChange) {
        this.onChange = onChange;
    },

    callOnSelect: function() {
        this.onSelect(this.current);
    },

    callOnChange: function() {
        this.onChange();
    },

    getValue: function() {
        var val = this.$valueInput.val();
        if (val) {
            return {id: val};
        } else {
            return {};
        }
    },

    clearValue: function() {
        this.$valueInput.val('');
    },

    clear: function() {
        this.$valueInput.val('');
        this.$textInput.val('');
    }
};
