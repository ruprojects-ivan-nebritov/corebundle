function BarcodeScanner(options) {
    this.self = this;

    this.settings = $.extend({
        onResult: this.onResult
    }, options);

    /**
     * Holds the actual scanned input
     */
    this.inputString = '';

    this.initScanner();
}


BarcodeScanner.prototype = {
    /**
     * Initiates the key press handler functions which trap key press input from the scanner
     */
    initScanner: function () {
        var self = this;
        $(document).keypress(function(e){
            self.keyHandler(e);
        });
    },

    /**
     * Handles Key Press Input
     *
     * @param e Event Object
     */
    keyHandler: function (e) {
        var code = e.keyCode;

        if (code == 13) {
            if (e.target.form !== undefined) {
                return;
            }
            e.preventDefault();
            var scan = this.inputString;
            this.inputString = '';
            var response = this.settings['onResult'];
            response(scan);
        } else {
            this.inputString += String.fromCharCode(e.charCode);
        }
    },

    setCallbackOnResult: function (callback) {
        this.settings['onResult'] = callback;
    },

    onResult: function (result) {
        $(document).trigger({type: 'scanned', scanResult: result});
    }
};