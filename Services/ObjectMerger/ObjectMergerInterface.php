<?php
namespace Rup\Bundle\CoreBundle\Services\ObjectMerger;

/**
 * Interface ObjectMergerInterface
 *
 * @package Rup\Bundle\CoreBundle\Services\ObjectMerger
 */
interface ObjectMergerInterface
{
    /**
     * Merges mixin into object
     *
     * @param mixed $object merge destination
     * @param mixed $mixin  admixing object
     */
    public function merge($object, $mixin);
}
