<?php
namespace Rup\Bundle\CoreBundle\Services\ObjectMerger\Metadata;

use Doctrine\Common\Util\ClassUtils;
use ReflectionClass;

/**
 * Class ReflectionMetadataCatcher
 *
 * @package Rup\Bundle\CoreBundle\Services\ObjectMerger\Metadata
 */
class ReflectionMetadataCatcher implements MetadataCatcherInterface
{
    /**
     * Returns an array names of the properties
     *
     * @param mixed $object
     *
     * @return string[]
     */
    public function getProperties($object)
    {
        $reflection = new ReflectionClass(ClassUtils::getRealClass(get_class($object)));

        return array_map(
            function ($property) {
                return $property['name'];
            },
            $reflection->getProperties()
        );
    }
}
