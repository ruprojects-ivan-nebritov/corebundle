<?php
namespace Rup\Bundle\CoreBundle\Exception\Controller;

use Rup\Bundle\CoreBundle\Exception\InvalidArgumentException;

/**
 * Class UnimplementedMethodException
 *
 * @package Rup\Bundle\CoreBundle\Exception\Controller
 */
class UnimplementedMethodException extends InvalidArgumentException
{

} 