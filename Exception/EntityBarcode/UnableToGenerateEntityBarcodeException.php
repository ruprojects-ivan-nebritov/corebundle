<?php
namespace Rup\Bundle\CoreBundle\Exception\EntityBarcode;

/**
 * Class UnableToGenerateEntityBarcodeException
 *
 * @package Rup\Bundle\CoreBundle\Exception\EntityBarcode
 */
class UnableToGenerateEntityBarcodeException extends EntityBarcodeException
{

} 