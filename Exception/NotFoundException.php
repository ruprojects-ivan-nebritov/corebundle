<?php
namespace Rup\Bundle\CoreBundle\Exception;

/**
 * Class NotFoundException
 *
 * @package Rup\Bundle\CoreBundle\Exception
 */
class NotFoundException extends \InvalidArgumentException implements Exception
{

}
