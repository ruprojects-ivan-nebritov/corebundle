<?php
namespace Rup\Bundle\CoreBundle\Exception;

/**
 * Class RuntimeException
 *
 * @package Rup\Bundle\CoreBundle\Exception
 */
class RuntimeException extends \RuntimeException implements Exception
{

}
