<?php
namespace Rup\Bundle\CoreBundle\Exception\Type;

use Rup\Bundle\CoreBundle\Exception\InvalidArgumentException;

/**
 * Class TypeException
 *
 * @package Rup\Bundle\CoreBundle\Exception\Type
 */
class TypeException extends InvalidArgumentException
{

}
