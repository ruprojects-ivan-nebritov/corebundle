<?php
namespace Rup\Bundle\CoreBundle\Model\EntityNumeration;

/**
 * Class BaseEntityNumeration
 *
 * @package Rup\Bundle\CoreBundle\Model\EntityNumeration
 */
abstract class BaseEntityNumeration
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $entityNumber;

    /**
     * @var string
     */
    protected $entityName;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $entityNumber
     *
     * @return BaseEntityNumeration
     */
    public function setEntityNumber($entityNumber)
    {
        $this->entityNumber = $entityNumber;

        return $this;
    }

    /**
     * @return int
     */
    public function getEntityNumber()
    {
        return $this->entityNumber;
    }

    /**
     * @param string $entityName
     *
     * @return BaseEntityNumeration
     */
    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }
}
