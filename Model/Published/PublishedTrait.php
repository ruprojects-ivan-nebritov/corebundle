<?php
namespace Rup\Bundle\CoreBundle\Model\Published;

/**
 * Class PublishedTrait
 *
 * @package Rup\Bundle\CoreBundle\Model\Published
 */
trait PublishedTrait
{
    /**
     * @var boolean $createDate
     */
    private $published = false;

    /**
     * @param boolean $published
     *
     * @return mixed
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isPublished()
    {
        return $this->published;
    }
}