<?php
namespace Rup\Bundle\CoreBundle\Repository;

use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\QueryBuilder as BaseQueryBuilder;
use Rup\Bundle\CoreBundle\Exception\InvalidArgumentException;
use Rup\Bundle\CoreBundle\Utils\StringUtils;

/**
 * Class QueryBuilder
 *
 * @package Rup\Bundle\CoreBundle\Repository
 */
class QueryBuilder extends BaseQueryBuilder
{

    /**
     * Example:
     * <code>
     *     $queryBuilder->addFieldTerm(
     *         array('u.firstName', 'u.lastName', 'u.middleName'),
     *         'John Doe',
     *         true,
     *         ' ',
     *         10
     *     );
     * </code>
     *
     * @param string|array $fields           must be defined in format "alias.fieldName"
     * @param string       $value
     * @param bool         $anyMatching      if true then LIKE-expression will be surrounded with %
     * @param string       $explodeDelimiter splitting substring
     *
     * @throws InvalidArgumentException
     */
    public function addFieldTerm($fields, $value, $anyMatching = false, $explodeDelimiter = ' ')
    {
        if (is_string($fields)) {
            $fields = array($fields);
        }

        $explodeLimit = count($fields);

        $words = StringUtils::splitWords($value, $explodeDelimiter, $explodeLimit);
        $where = $this->expr()->orX();

        foreach ($fields as $field) {
            $orWhere = $this->expr()->orX();

            foreach ($words as $wordIndex => $word) {
                $parameter = $this->createParameterName($field, $wordIndex);

                $like = $this->createLike($field, $word, $parameter, $anyMatching);

                $orWhere->add($like);
            }

            $where->add($orWhere);
        }

        $this->andWhere($where);
    }

    /**
     * @param string $field should be defined in format "alias.fieldName"
     * @param array $values
     */
    public function addIn($field, $values)
    {
        if (is_array($values) && count($values)) {
            $in = $this->expr()->in($field, $values);
            $this->andWhere($in);
        }
    }

    /**
     * @param string $field should be defined in format "alias.fieldName"
     * @param array $values
     */
    public function addNotIn($field, $values)
    {
        if (is_array($values) && count($values)) {
            $notIn = $this->expr()->notIn($field, $values);
            $this->andWhere($notIn);
        }
    }

    /**
     * Add types/status select
     *
     * @param string $field should be defined in format "alias.fieldName"
     * @param array $types
     */
    public function addTypes($field, $types)
    {
        $orExpr = $this->expr()->orX();

        foreach ($types as $type) {
            $parameter = $this->createParameterName($field, 'type_' . $type);

            $orExpr->add("$field = :$parameter");
            $this->setParameter($parameter, $type);
        }

        $this->andWhere($orExpr);
    }

    /**
     * @param string $field should be defined in format "alias.fieldName"
     * @param int $rangeFrom
     * @param int $rangeTo
     */
    public function addRange($field, $rangeFrom, $rangeTo)
    {
        $range = $this->expr()->between($field, $rangeFrom, $rangeTo);

        $this->andWhere($range);
    }

    /**
     * @param string $field
     * @param string $value
     * @param string $parameterName
     * @param bool $anyMatching
     *
     * @throws InvalidArgumentException
     *
     * @return Comparison
     */
    protected function createLike($field, $value, $parameterName = null, $anyMatching = false)
    {
        if (!is_string($field) || !is_string($field)) {
            throw new InvalidArgumentException(sprintf("Given field or value is not string in %s", __METHOD__));
        }

        if (!$parameterName) {
            $parameterName = $this->createParameterName($field);
        }

        $like = $this->expr()->like($field, ":$parameterName");

        $value = ($anyMatching) ? '%' . $value . '%' : $value . '%';

        $this->setParameter($parameterName, $value);

        return $like;
    }

    /**
     * @param $field
     * @param null $fieldSalt
     *
     * @return string
     */
    protected function createParameterName($field, $fieldSalt = null)
    {
        return 'field_' . str_replace('.', '_', $field) . '_' . $fieldSalt;
    }
}
