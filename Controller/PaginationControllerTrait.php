<?php
namespace Rup\Bundle\CoreBundle\Controller;

use Porpaginas\Doctrine\ORM\ORMQueryPage;
use Porpaginas\Result;

/**
 * Class PaginationControllerTrait
 *
 * @package Rup\Bundle\CoreBundle\Controller
 */
trait PaginationControllerTrait
{
    /**
     * @param Result $result
     *
     * @return ORMQueryPage
     */
    protected function getPagination(Result $result)
    {
        $page       = $this->get('request')->query->get('page', 1) - 1;
        $pagination = $result->take(
            $page * $this->getPerPage(),
            $this->getPerPage()
        );

        return $pagination;
    }

    /**
     * @param ORMQueryPage $pagination
     *
     * @return int
     */
    protected function getNumberOfPreviousItemsForPagination(ORMQueryPage $pagination)
    {
        return ($pagination->getCurrentPage() - 1) * $pagination->getCurrentLimit();
    }

    /**
     * @return int
     */
    protected function getPerPage()
    {
        return $this->container->getParameter('per_page');
    }
} 