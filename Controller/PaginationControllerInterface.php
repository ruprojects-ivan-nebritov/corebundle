<?php
namespace Rup\Bundle\CoreBundle\Controller;

use Porpaginas\Doctrine\ORM\ORMQueryPage;
use Porpaginas\Result;

/**
 * Interface PaginationControllerInterface
 *
 * @package Rup\Bundle\CoreBundle\Controller
 */
interface PaginationControllerInterface
{
    /**
     * @param Result $result
     *
     * @return ORMQueryPage
     */
    public function getPagination(Result $result);

    /**
     * @param ORMQueryPage $pagination
     *
     * @return int
     */
    public function getNumberOfPreviousItemsForPagination(ORMQueryPage $pagination);

    /**
     * @return int
     */
    public function getPerPage();
} 