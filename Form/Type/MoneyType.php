<?php
namespace Rup\Bundle\CoreBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\MoneyType as BasicMoneyType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * Class MoneyType
 *
 * @package Rup\Bundle\CoreBundle\Form\Type
 */
class MoneyType extends BasicMoneyType
{
    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['currency'] = $this->getCurrency($options['currency']);
    }

    private function getCurrency($currency)
    {
        $locale = \Locale::getDefault();

        $format = new \NumberFormatter($locale, \NumberFormatter::CURRENCY);
        $pattern = $format->formatCurrency('123', $currency);

        preg_match('/^([^\s\xc2\xa0]*)[\s\xc2\xa0]*123(?:[,.]0+)?[\s\xc2\xa0]*([^\s\xc2\xa0]*)$/u', $pattern, $matches);

        if (!empty($matches[1])) {
            return $matches[1];
        } elseif (!empty($matches[2])) {
            return $matches[2];
        } else {
            return $currency;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rup_money';
    }
}