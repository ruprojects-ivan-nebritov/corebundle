<?php
namespace Rup\Bundle\CoreBundle\Form\Type;

use Rup\Bundle\CoreBundle\Form\DataTransformer\TimestampTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class TimestampType
 */
class TimestampType extends AbstractType
{
    /**
     * @var string
     */
    protected $format;

    /**
     * @param string $format
     */
    public function __construct($format)
    {
        $this->format = $format;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new TimestampTransformer($options['format'], $options['time']));
    }

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'format'
        ));

        $resolver->setDefaults(array(
            'format'         => $this->format,
            'input'          => 'datetime',
            'by_reference'   => false,
            'error_bubbling' => false,
            'compound'       => false,
            'time'           => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'timestamp';
    }
}
