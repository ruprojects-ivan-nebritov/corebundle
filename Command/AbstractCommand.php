<?php
namespace Rup\Bundle\CoreBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Abstract command class with helpers
 */
abstract class AbstractCommand extends ContainerAwareCommand
{

    // ------------ Input/Output ----------------- //

    /**
     * Return string for request
     *
     * @param string $question
     * @param mixed  $default
     * @param string $sep
     *
     * @return string
     */
    public function getQuestion($question, $default, $sep = ':')
    {
        return $default ? sprintf('<info>%s</info> [<comment>%s</comment>]%s ', $question, $default, $sep) : sprintf(
            '<info>%s</info>%s ',
            $question,
            $sep
        );
    }

    /**
     * Return string for confirm
     *
     * @param string $question
     * @param mixed  $default
     * @param string $sep
     *
     * @return string
     */
    public function getQuestionConfirm($question, $default = 'Y/n', $sep = ':')
    {
        return $default ? sprintf('<question>%s</question> [<comment>%s</comment>]%s ', $question, $default,
            $sep) : sprintf(
            '<info>%s</info>%s ',
            $question,
            $sep
        );
    }

    /**
     * Outputs information
     *
     * @param OutputInterface $output
     * @param string          $message
     */
    protected function info($output, $message)
    {
        $output->writeln('<info>' . $message . '</info>');
    }

    /**
     * Outputs error
     *
     * @param OutputInterface $output
     * @param string          $message
     */
    protected function error($output, $message)
    {
        $output->writeln('<error>' . $message . '</error>');
    }

    // ------------ Services ----------------- //

    /**
     * @return EntityManager
     */
    protected function getManager()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * @return ValidatorInterface
     */
    protected function getValidator()
    {
        return $this->getContainer()->get('validator');
    }

    // ------------ Various ----------------- //

    /**
     * Validates value
     *
     * @param mixed $value
     * @param array $constraints
     *
     * @throws \InvalidArgumentException
     */
    protected function validate($value, array $constraints)
    {
        $validator = $this->getValidator();

        /** @var ConstraintViolation[] $errorList */
        $errorList = $validator->validate($value, $constraints);

        $errors = null;
        foreach ($errorList as $error) {
            throw new \InvalidArgumentException($error->getMessage());
        }
    }


}